#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8;
import completeCardDeck
import cardStack

class cardDesk():
	def __init__(self):
		self.cardDeck = completeCardDeck.completeCardDeck()
		self.centralCardStack = cardStack.cardStack();
		self.players = list()
		
	def takeASeat(player):
		if len(self.players) < 4:
			self.players.extend(player)
		else:
			raise ValueError('Der Tisch ist bereits voll')
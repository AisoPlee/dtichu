#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8
from random import shuffle
import abstracts
import cardHand

class completeCardDeck():
	def __init__(self):
		self.allCards = list()
		for i in range (4):
			color = abstracts.CardColor(i+1)
			for u in range (13):
				card = abstracts.regularCard(color,u+1)
				self.allCards.extend([card])
		dragon = abstracts.specialCard(abstracts.CardColor.SPECIAL_YELLOW,abstracts.SpecialValues.DRAGON)
		phönix = abstracts.specialCard(abstracts.CardColor.SPECIAL_YELLOW,abstracts.SpecialValues.PHÖNIX)
		dog = abstracts.specialCard(abstracts.CardColor.SPECIAL_YELLOW,abstracts.SpecialValues.DOG)
		bird = abstracts.specialCard(abstracts.CardColor.SPECIAL_YELLOW,abstracts.SpecialValues.BIRD)
		self.allCards.extend([dragon])
		self.allCards.extend([phönix])
		self.allCards.extend([dog])
		self.allCards.extend([bird])
		
	def giveCards():
		shuffle(self.allCards)
		cardHands=list();
		one = cardHand.CardHand(self.allCards[0:14])
		two = cardHand.CardHand(self.allCards[14:28])
		three = cardHand.CardHand(self.allCards[28:42])
		four = cardHand.CardHand(self.allCards[42:56])	
		cardHands.extend(one)
		cardHands.extend(two)
		cardHands.extend(three)
		cardHands.extend(four)
		return cardHands();


#!/usr/bin/env python3
# This Python file uses the following encoding: utf-8
from aenum import Enum


class aCard():
	def __init__(self, c):
		self.color = c
		
class CardColor(Enum):
	GREEN = 1
	BLACK = 2
	RED = 3
	BLUE = 4
	SPECIAL_YELLOW = 5
	
class SpecialValues(Enum):
	DRAGON = 1
	PHÖNIX = 2
	DOG = 3
	BIRD = 4
	
class regularCard(aCard):
	def __init__(self, c, v):
		super().__init__(c)
		self.value = v

class specialCard(aCard):
	def __init__(self, c, v):
		super().__init__(c)
		self.value = v
		